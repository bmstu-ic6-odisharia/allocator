/* gcc -fPIC -shared allocator.c -o allocator.so
** LD_PRELOAD=/path/to/malloc.so
** export LD_PRELOAD */

#include <sys/mman.h>                           // mmap
#include <unistd.h>                             // get pagesize
#include <string.h>                             // for memcpy
#include <pthread.h>                            // mutex
#include <sys/types.h>
#include <stdio.h>

struct block_control {
    size_t  size;
    void*   prev_chunk;
    void*   next_chunk;
};

struct page_control_vs {
    void*   next_page;
    char    chunk_card[62];
};

struct page_control_s {
    void*   next_page;
    char    chunk_card[31];
};

struct page_control {
    void*   next_page;
    void*   used_blocks;
    void*   freed_blocks;
};

struct page_control_l {
    void*   next_page;
    size_t  size;
};

// static void  malloc_init(void);
void  free(void *fbyte);
void* malloc(size_t bytes);
void* calloc(size_t number, size_t size);
void* realloc(void *ptr, size_t size);

static void * very_small_chunks_page_pointer = NULL;
static void * small_chunks_page_pointer      = NULL;
static void * big_chunks_page_pointer        = NULL;
static void * large_chunks_page_pointer      = NULL;

static const size_t VERY_SMALL_CHUNKS_LIMIT       = 64;     // size limit for very small chunks
static const size_t SMALL_CHUNKS_LIMIT            = 128;    // size limit for small chunks

static const char VERY_SMALL_PAGES = 0x10;
static const char SMALL_PAGES      = 0x11;

pthread_mutex_t lock;                   // mutex

int malloc_init_any_small_chunks(char num_chunks, char type){
    long PAGE_SIZE = sysconf(_SC_PAGESIZE);       // pagesize in the current system

    void * any_small_chunks_page_pointer;
    struct page_control_vs * vs_pg_ptr = NULL;
    struct page_control_s * s_pg_ptr = NULL;

    if (type == VERY_SMALL_PAGES){
        very_small_chunks_page_pointer = mmap(NULL, PAGE_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
        any_small_chunks_page_pointer = very_small_chunks_page_pointer;
    }
    else {
        small_chunks_page_pointer = mmap(NULL, PAGE_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
        any_small_chunks_page_pointer = small_chunks_page_pointer;
    }

    if (any_small_chunks_page_pointer == (void*) -1){
        return -1;
    }

    if (type == VERY_SMALL_PAGES){
        vs_pg_ptr = (struct page_control_vs*) very_small_chunks_page_pointer;
        vs_pg_ptr->next_page = NULL;
        for (char i = 0; i < num_chunks; i++) {
            vs_pg_ptr->chunk_card[i] = 0;
        }
    }
    else {
        s_pg_ptr = (struct page_control_s*) small_chunks_page_pointer;
        s_pg_ptr->next_page = NULL;
        for (char i = 0; i < num_chunks; i++) {
            s_pg_ptr->chunk_card[i] = 0;
        }
    }

    return 0;
}

void* malloc_add_page_any_small_chunks(char num_chunks, char type){
    long PAGE_SIZE = sysconf(_SC_PAGESIZE);       // pagesize in the current system

    struct page_control_vs* vs_pg_ptr = (struct page_control_vs*) very_small_chunks_page_pointer;
    struct page_control_s* s_pg_ptr = (struct page_control_s*) small_chunks_page_pointer;

    void* new_page_addr = mmap(NULL, PAGE_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    if (new_page_addr == (void*) -1){
        return NULL;
    }

    if (type == VERY_SMALL_PAGES){
        struct page_control_vs* vs_pg_tmp_ptr;
        // Found last page and append
        while (vs_pg_ptr->next_page) {
            vs_pg_ptr = (struct page_control_vs*) vs_pg_ptr->next_page;
        }
        vs_pg_ptr->next_page = new_page_addr;
        // Set page_control structure
        vs_pg_tmp_ptr = (struct page_control_vs*) new_page_addr;
        vs_pg_tmp_ptr->next_page = NULL;
        for (size_t i = 0; i < num_chunks; i++) {
            vs_pg_tmp_ptr->chunk_card[i] = 0;
        }
    }
    else {
        struct page_control_s* s_pg_tmp_ptr;
        // Found last page and append
        while (s_pg_ptr->next_page) {
            s_pg_ptr = (struct page_control_s*) s_pg_ptr->next_page;
        }
        s_pg_ptr->next_page = new_page_addr;
        // Set page_control structure
        s_pg_tmp_ptr = (struct page_control_s*) new_page_addr;
        s_pg_tmp_ptr->next_page = NULL;
        for (size_t i = 0; i < num_chunks; i++) {
            s_pg_tmp_ptr->chunk_card[i] = 0;
        }
    }

    return new_page_addr;
}

int malloc_init_big_chunks(){
    long PAGE_SIZE = sysconf(_SC_PAGESIZE);     // pagesize in the current system
    PAGE_SIZE = 4096;

    struct page_control* current_addr;
    big_chunks_page_pointer = mmap(NULL, PAGE_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    if (big_chunks_page_pointer == (void*) -1){
        return -1;
    }
    current_addr = (struct page_control*) big_chunks_page_pointer;
    current_addr->next_page     = NULL;         // pointer to next_page
    current_addr->used_blocks   = NULL;         // pointer to used
    current_addr->freed_blocks  = NULL;         // pointer to freed
    return 0;
}

void* malloc_add_page_big_chunks(){
    long PAGE_SIZE = sysconf(_SC_PAGESIZE);       // pagesize in the current system

    struct page_control* b_pg_ptr = (struct page_control*) big_chunks_page_pointer;
    void* new_page_addr = mmap(NULL, PAGE_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    if (new_page_addr == (void*) -1){
        return NULL;
    }
    // Found last page and append
    while (b_pg_ptr->next_page) {
        b_pg_ptr = (struct page_control*) b_pg_ptr->next_page;
    }
    b_pg_ptr->next_page = new_page_addr;
    // Set page_control structure
    b_pg_ptr = (struct page_control*) new_page_addr;
    b_pg_ptr->next_page       = NULL;
    b_pg_ptr->used_blocks     = NULL;
    b_pg_ptr->freed_blocks    = NULL;
    return new_page_addr;
}

int malloc_init_large_chunks(size_t length){
    struct page_control_l* l_pg_ptr;
    large_chunks_page_pointer = mmap(NULL, length + sizeof(struct page_control_l), PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    if (large_chunks_page_pointer == (void*) -1){
        return -1;
    }
    l_pg_ptr = (struct page_control_l*) large_chunks_page_pointer;
    l_pg_ptr->next_page = NULL;
    l_pg_ptr->size = length;
    return 0;
}

void* malloc_add_page_large_chunks(size_t length){
    struct page_control_l* l_pg_ptr = (struct page_control_l*) large_chunks_page_pointer;
    void* new_page_addr = mmap(NULL, length + sizeof(struct page_control_l), PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    if (new_page_addr == (void*) -1){
        return NULL;
    }
    // Found last page and append
    while (l_pg_ptr->next_page) {
        l_pg_ptr = (struct page_control_l*) l_pg_ptr->next_page;
    }
    l_pg_ptr->next_page = new_page_addr;
    // Set page_control structure
    l_pg_ptr = (struct page_control_l*) new_page_addr;
    l_pg_ptr->next_page = NULL;
    l_pg_ptr->size      = length;
    return new_page_addr;
}

void vs_pg_chk(void* pg_ptr, long PAGE_SIZE){
    struct page_control_vs* vs_pg_ptr = (struct page_control_vs*) pg_ptr;
    struct page_control_vs* vs_pg_tmp_ptr = (struct page_control_vs*) very_small_chunks_page_pointer;
    for (size_t i = 0; i < 62; i++){
        if (vs_pg_ptr->chunk_card[i] == 1){
            return;
        }
    }
    if ( (void *) very_small_chunks_page_pointer == (void *) pg_ptr ){
        very_small_chunks_page_pointer = vs_pg_ptr->next_page;
    }
    else {
        while ( (void *) vs_pg_tmp_ptr->next_page < (void *) pg_ptr ){
            vs_pg_tmp_ptr = (struct page_control_vs*) vs_pg_tmp_ptr->next_page;
        }
        vs_pg_tmp_ptr->next_page = vs_pg_ptr->next_page;
    }
    munmap(pg_ptr, PAGE_SIZE);
    return;
};

void s_pg_chk(void* pg_ptr, long PAGE_SIZE){
    struct page_control_s* s_pg_ptr = (struct page_control_s*) pg_ptr;
    struct page_control_s* s_pg_tmp_ptr = (struct page_control_s*) small_chunks_page_pointer;
    for (size_t i = 0; i < 31; i++){
        if (s_pg_ptr->chunk_card[i] == 1){
            return;
        }
    }
    if ( (void *) small_chunks_page_pointer == (void *) pg_ptr ){
        small_chunks_page_pointer = s_pg_ptr->next_page;
    }
    else {
        while ( (void *) s_pg_tmp_ptr->next_page < (void *) pg_ptr ){
            s_pg_tmp_ptr = (struct page_control_s*) s_pg_tmp_ptr->next_page;
        }
        s_pg_tmp_ptr->next_page = s_pg_ptr->next_page;
    }
    munmap(pg_ptr, PAGE_SIZE);
    return;
};

void free(void* pointer){
    long PAGE_SIZE = sysconf(_SC_PAGESIZE);       // pagesize in the current system

    if (! pointer){
        return;
    }
    pthread_mutex_lock(&lock);
    void* current_page;
    void* current_chunk;
    void* current_addr;
    char  found_in_big = 0;

    // Check whether pointer in very_small_pages
    if (very_small_chunks_page_pointer){
        struct page_control_vs* vs_pg_ptr;
        current_page = very_small_chunks_page_pointer;
        while (current_page){
            if ( (void *) pointer >= (void *) current_page + sizeof(struct page_control_vs) &&
                 (void *) pointer <  (void *) current_page + PAGE_SIZE ){
                char number = (char) (((void *) pointer - ((void *) current_page + sizeof(struct page_control_vs))) / VERY_SMALL_CHUNKS_LIMIT);
                vs_pg_ptr = (struct page_control_vs*) current_page;
                vs_pg_ptr->chunk_card[number] = 0;
                vs_pg_chk((void *) vs_pg_ptr, PAGE_SIZE);
                goto FREE_END;
            }
            current_page = ((struct page_control_vs*) current_page)->next_page;
        }
    }

    // Check whether pointer in small_pages
    if (small_chunks_page_pointer){
        struct page_control_s* s_pg_ptr;
        current_page = (struct page_control_s*) small_chunks_page_pointer;
        while (current_page){
            if ( (void *) pointer >= (void *) current_page + sizeof(struct page_control_s) &&
                 (void *) pointer <  (void *) current_page + PAGE_SIZE){
                char number = (char) (((void *) pointer - ((void *) current_page + sizeof(struct page_control_s))) / SMALL_CHUNKS_LIMIT);
                s_pg_ptr = (struct page_control_s*) current_page;
                s_pg_ptr->chunk_card[number] = 0;
                s_pg_chk(s_pg_ptr, PAGE_SIZE);
                goto FREE_END;
            }
            current_page = ((struct page_control_s*) current_page)->next_page;
        }
    }

    // Check whether pointer in big_pages
    if (big_chunks_page_pointer){
        current_page = (struct page_control*) big_chunks_page_pointer;          // pointer to page

        while (current_page){
            current_chunk = ((struct page_control*) current_page)->used_blocks;
            while (current_chunk){
                if (pointer == current_chunk + sizeof(struct block_control)){
                    found_in_big = 1;
                    break;
                }
                current_chunk = ((struct block_control*) current_chunk)->next_chunk;
            }
            if (found_in_big){
                break;
            }
            current_page = ((struct page_control*) current_page)->next_page;
        }

        if (found_in_big){
            struct block_control* temp_prev_chunk   = (struct block_control*) ((struct block_control*) current_chunk)->prev_chunk;
            struct block_control* temp_next_chunk   = (struct block_control*) ((struct block_control*) current_chunk)->next_chunk;
            struct block_control* current_free_chunk = (struct block_control*) ((struct page_control*) current_page)->freed_blocks;
            struct block_control* current_used_chunk;
            struct block_control* temp_free_chunk;
            struct page_control* b_pg_tmp_ptr;

            // Ummap page, if chunk was the only busy chunk on the page and there
            // were not any freed chunks
            if (!temp_prev_chunk && !temp_next_chunk){
                b_pg_tmp_ptr = (struct page_control*) big_chunks_page_pointer;
                while (b_pg_tmp_ptr->next_page < current_page){
                    b_pg_tmp_ptr = (struct page_control*) b_pg_tmp_ptr->next_page;
                }
                b_pg_tmp_ptr->next_page = ((struct page_control*) current_page)->next_page;
                munmap(current_page, PAGE_SIZE);
                goto FREE_END;
            }

            // Remove chunk from list of used blocks
            struct block_control* temp_current_chunk = (struct block_control*) current_chunk;
            if (temp_prev_chunk){
                temp_prev_chunk->next_chunk = (void *) ((struct block_control*) current_chunk)->next_chunk;
            } else {
                b_pg_tmp_ptr = (struct page_control*) current_page;
                b_pg_tmp_ptr->used_blocks = (void *) temp_next_chunk;
            }
            if (temp_next_chunk){
                temp_next_chunk->prev_chunk = (void *) ((struct block_control*) current_chunk)->prev_chunk;
            }
            temp_current_chunk->next_chunk = NULL;
            temp_current_chunk->prev_chunk = NULL;

            current_used_chunk = (struct block_control*) ((struct page_control*) current_page)->used_blocks;
            // Found address of chunk with the largest address in the list of used chunks
            while (current_used_chunk->next_chunk) {
                current_used_chunk = (struct block_control*) current_used_chunk->next_chunk;
            }

            // Insert in tle list of freed. If free blocks is not empty, just add
            // in right place of list.
            if (current_free_chunk){

                // Go through list of free chunks. When address of chunk become
                // more than our block, it's time to insert in tle list.
                while ( (void *) current_free_chunk < (void *) current_chunk){
                    current_free_chunk = (struct block_control*) current_free_chunk->next_chunk;
                }

                // If freed chunk has larger address than head of used chunks list
                if ( (void *) current_used_chunk < (void *) current_chunk){
                    temp_free_chunk = (struct block_control*) ((struct page_control*) current_page)->freed_blocks;

                    // Everything is OK while free chunk has address of the next element
                    // in the list of freed chunks
                    while ( (void *) temp_free_chunk->next_chunk < (void *) current_used_chunk){
                        temp_free_chunk = (struct block_control*) temp_free_chunk->next_chunk;
                    }

                    // When address of the next element of free chunk list is larger
                    // than address of the head of used chunks list it's time to
                    // chomp the list
                    temp_free_chunk->next_chunk = NULL;
                    goto FREE_END;
                }

                // If freed chunk inserts in the beginning
                if (current_free_chunk->prev_chunk == NULL){
                    struct block_control* temp_chunk = (struct block_control*) current_chunk;
                    current_free_chunk->prev_chunk = (void *) temp_chunk;
                    temp_chunk->next_chunk = (void *) current_free_chunk;
                    temp_chunk->prev_chunk = NULL;
                    struct page_control* current_page_casted = (struct page_control*) current_page;
                    current_page_casted->freed_blocks = temp_chunk;
                    goto FREE_END;
                }

                // Just insert into the list. Regular insertion.
                if (current_free_chunk->next_chunk){
                    struct block_control* current_chunk_casted = (struct block_control*) current_chunk;
                    current_chunk_casted->next_chunk = (void *) current_free_chunk;
                    current_chunk_casted->prev_chunk = (void *) current_free_chunk->next_chunk;
                    current_free_chunk->prev_chunk = (void *) current_chunk_casted;
                    goto FREE_END;
                }

                // We should append
                if (current_free_chunk = NULL){
                    struct page_control* current_page_casted = (struct page_control*) current_page;
                    struct block_control* current_free_chunk = (struct block_control*) ((struct page_control*) current_page)->freed_blocks;
                    struct block_control* current_chunk_casted = (struct block_control*) current_chunk;
                    while (current_free_chunk->next_chunk){
                        current_free_chunk = (struct block_control*) current_free_chunk->next_chunk;
                    }
                    current_free_chunk->next_chunk = (void *) current_chunk;
                    current_chunk_casted->prev_chunk = (void *) current_free_chunk;
                    goto FREE_END;
                }
            }
            else {
                struct page_control* current_page_casted = (struct page_control*) current_page;
                current_page_casted->freed_blocks = current_chunk;
                goto FREE_END;
            }
        }
    }

    // Check whether pointer in large_pages
    if (large_chunks_page_pointer){
        struct page_control_l* current_page = (struct page_control_l*) large_chunks_page_pointer;
        struct page_control_l* temp_page = (struct page_control_l*) large_chunks_page_pointer;
        while ((pointer != current_page + sizeof(struct page_control_l))
                    && (current_page)){
            current_page = (struct page_control_l*) current_page->next_page;
        }
        if (current_page){
            while ( (void *) temp_page->next_page < (void *) current_page){
                temp_page = (struct page_control_l*) temp_page->next_page;
            }
            temp_page->next_page = current_page->next_page;
            munmap(current_page, current_page->size + sizeof(struct page_control_l));
        }
    }

    FREE_END:
    pthread_mutex_unlock(&lock);
    return;
}

void* malloc(size_t block_size) {
    long PAGE_SIZE = sysconf(_SC_PAGESIZE);       // pagesize in the current system

    if (!block_size){
        return NULL;
    }

    void* memory_location       = NULL;         //  pointer to new memory
    void* current_addr          = NULL;
    void* current_page          = NULL;
    char  found_in_free         = 0;
    char  found_space_in_pages  = 0;

    struct block_control* current_chunk = NULL;
    struct block_control* found_block   = NULL;
    pthread_mutex_lock(&lock);

    block_size += 8 - (block_size % 8);              // for memory alignment

    if (block_size <= VERY_SMALL_CHUNKS_LIMIT) {
        // working with pages fpr very small chunks
        struct page_control_vs* vs_pg_ptr;
        if (very_small_chunks_page_pointer == NULL) {               // init pointer
            if (malloc_init_any_small_chunks(62, VERY_SMALL_PAGES) == -1){
                memory_location = NULL;
                goto MALLOC_END;
            }
            vs_pg_ptr = (struct page_control_vs*) very_small_chunks_page_pointer;
            vs_pg_ptr->chunk_card[0] = 1;
            memory_location = (void *) vs_pg_ptr + sizeof(struct page_control_vs) + 0 * VERY_SMALL_CHUNKS_LIMIT;
            goto MALLOC_END;
        }

        vs_pg_ptr = (struct page_control_vs*) very_small_chunks_page_pointer;
        while (vs_pg_ptr){
            char i = 0;
            while (i < 62 && vs_pg_ptr->chunk_card[i] == 1){
                i++;
            }
            if (i != 62){
                found_in_free = 1;
                vs_pg_ptr->chunk_card[i] = 1;
                memory_location = (void *) vs_pg_ptr + sizeof(struct page_control_vs) + i * VERY_SMALL_CHUNKS_LIMIT;
                goto MALLOC_END;
            }
            vs_pg_ptr = (struct page_control_vs*) vs_pg_ptr->next_page;
        }

        if (! found_in_free){
            vs_pg_ptr = (struct page_control_vs*) malloc_add_page_any_small_chunks(62, VERY_SMALL_PAGES);
            if (vs_pg_ptr == NULL){
                memory_location = NULL;
                goto MALLOC_END;
            }
            vs_pg_ptr = (struct page_control_vs*) vs_pg_ptr;
            vs_pg_ptr->chunk_card[0] = 1;
            memory_location = (void *) vs_pg_ptr + sizeof(struct page_control_vs) + 0 * VERY_SMALL_CHUNKS_LIMIT;
            goto MALLOC_END;
        }
    } else

    if (block_size <= SMALL_CHUNKS_LIMIT) {
        // working with pages for small chunks
        struct page_control_s* s_pg_ptr;
        if (small_chunks_page_pointer == NULL) {               // init pointer
            if (malloc_init_any_small_chunks(31, SMALL_PAGES) == -1){
                memory_location = NULL;
                goto MALLOC_END;
            }
            s_pg_ptr = (struct page_control_s*) small_chunks_page_pointer;
            s_pg_ptr->chunk_card[0] = 1;
            memory_location = (void *) s_pg_ptr + sizeof(struct page_control_s) + 0 * SMALL_CHUNKS_LIMIT;
            goto MALLOC_END;
        }

        s_pg_ptr = (struct page_control_s*) small_chunks_page_pointer;
        while (s_pg_ptr){
            char i = 0;
            while (i < 31 && s_pg_ptr->chunk_card[i] == 1){
                i++;
            }
            if (i != 31){
                found_in_free = 1;
                s_pg_ptr->chunk_card[i] = 1;
                memory_location = (void *) s_pg_ptr + sizeof(struct page_control_s) + i * SMALL_CHUNKS_LIMIT;
                goto MALLOC_END;
            }
            s_pg_ptr = (struct page_control_s*) s_pg_ptr->next_page;
        }

        if (! found_in_free){
            s_pg_ptr = (struct page_control_s*) malloc_add_page_any_small_chunks(31, SMALL_PAGES);
            if (s_pg_ptr == NULL){
                memory_location = NULL;
                goto MALLOC_END;
            }
            s_pg_ptr = (struct page_control_s*) s_pg_ptr;
            s_pg_ptr->chunk_card[0] = 1;
            memory_location = (void *) s_pg_ptr + sizeof(struct page_control_s) + 0 * SMALL_CHUNKS_LIMIT;
            goto MALLOC_END;
        }
    } else

    if ((block_size > SMALL_CHUNKS_LIMIT) && (block_size <= PAGE_SIZE - sizeof(struct page_control))){
        // Working with big chunks
        struct page_control* b_pg_ptr;
        struct block_control* temp_chunk;
        struct block_control* current_chunk;
        struct block_control* found_block;
        // If pages of this type of data wasn't initialized, we should to do it
        // and allocate our block_size.
        if (big_chunks_page_pointer == NULL) {
            if (malloc_init_big_chunks() == -1){
                memory_location = NULL;
                goto MALLOC_END;
            }
            b_pg_ptr = (struct page_control*) big_chunks_page_pointer;
            found_block = (struct block_control*) ((void *) big_chunks_page_pointer + sizeof(struct page_control));
            b_pg_ptr->used_blocks = (void *) found_block;
            found_block->prev_chunk = NULL;
            found_block->next_chunk = NULL;
            found_block->size = block_size;
            memory_location = (void *) found_block + sizeof(struct block_control);
            goto MALLOC_END;
        }

        // Find block in the list of free blocks from every page from list of
        // pages. We stop looking for it when size of freed chunk is greater
        // than or equal to the size of needed block.
        b_pg_ptr = (struct page_control*) big_chunks_page_pointer;          // pointer to page
        while (b_pg_ptr){
            current_chunk = (struct block_control*) b_pg_ptr->freed_blocks; // pointer to list of free blocks
            while (current_chunk){
                if (current_chunk->size >= block_size){
                    found_in_free = 1;
                    found_block = current_chunk;

                    // If found block has prev_chunk, we need to reassign
                    // prev_chunk and next_chunk of next and previous blocks.
                    // Otherwise we just reassign base pointer of a page to the
                    // next_chunk field of a found_block
                    if (found_block->prev_chunk){
                        struct block_control* temp_chunk = (struct block_control*) found_block->prev_chunk;
                        temp_chunk->next_chunk = (void *) found_block->next_chunk;
                        if (found_block->next_chunk){
                            temp_chunk = (struct block_control*) found_block->next_chunk;
                            temp_chunk->prev_chunk = (void *) found_block->prev_chunk;
                        }
                    }
                    else {
                        b_pg_ptr->freed_blocks = found_block->next_chunk;
                    }

                    // Find used chunk which next_chunk address greater than
                    // address of our found chunk. Then we insert our chunk into
                    // the list
                    current_chunk = (struct block_control*) b_pg_ptr->used_blocks;
                    while (current_chunk->next_chunk && (void *) current_chunk->next_chunk < (void *) found_block){
                        current_chunk = (struct block_control*) current_chunk->next_chunk;
                    }
                    if ( (void *) current_chunk > (void *) found_block ){
                        b_pg_ptr->used_blocks = (void *) found_block;
                        found_block->next_chunk = (void *) current_chunk;
                    }
                    else {
                        found_block->next_chunk = (void *) current_chunk->next_chunk;
                        found_block->prev_chunk = (void *) current_chunk;
                        current_chunk->next_chunk = (void *) found_block;
                        if (found_block->next_chunk){
                            temp_chunk = (struct block_control*) found_block->next_chunk;
                            temp_chunk->prev_chunk = (void *) found_block;
                        }
                    }
                    memory_location = (void *) found_block + sizeof(struct block_control);

                    break;
                }
                current_chunk = (struct block_control*) current_chunk->next_chunk;
            }
            if (found_in_free){
                break;
            }
            b_pg_ptr = (struct page_control*) b_pg_ptr->next_page;
        }

        // Otherwise we trying to allocate block in the pages appending to the used_
        // blocks list if space between end of the every page and last used chunk
        // is more than required block_size. Otherwise we request new page from
        // kernel and allocate our block_size at new page
        if (! found_in_free){
            // Trying to find page with enough free space to allocate our block.
            // Otherwise we must to call mmap to allocate new memory page.
            b_pg_ptr = (struct page_control*) big_chunks_page_pointer;                      // pointer to page
            while (b_pg_ptr){
                // Go up to the end of list of used chunks
                current_chunk = (struct block_control*) b_pg_ptr->used_blocks;              // pointer to list of free blocks
                while (current_chunk->next_chunk){
                    current_chunk = (struct block_control*) current_chunk->next_chunk;
                }
                // If we have enough space we append. Otherwise we go to the next
                // page. And so on.
                if ((void *) b_pg_ptr + PAGE_SIZE - ((void *) current_chunk + current_chunk->size)
                    > block_size + sizeof(struct block_control)){
                    found_block = (struct block_control*) ((void *) current_chunk + current_chunk->size);
                    found_block->prev_chunk = (void *) current_chunk;
                    found_block->next_chunk = NULL;
                    found_block->size = block_size;
                    current_chunk->next_chunk = (void *) found_block;
                    found_space_in_pages = 1;
                    memory_location = (void *) found_block + sizeof(struct block_control);
                    break;
                }
                b_pg_ptr = (struct page_control*) b_pg_ptr->next_page;
            }

            if (! found_space_in_pages){
                b_pg_ptr = (struct page_control*) malloc_add_page_big_chunks();
                if (b_pg_ptr == NULL){
                    memory_location = NULL;
                    goto MALLOC_END;
                }
                found_block = (struct block_control*) ((void *) b_pg_ptr + sizeof(struct page_control));
                b_pg_ptr->used_blocks = (void *) found_block;
                found_block->prev_chunk = NULL;
                found_block->next_chunk = NULL;
                found_block->size = block_size;
                memory_location = (void *) found_block + sizeof(struct page_control);
                goto MALLOC_END;
            }
        }

    } else

    if (block_size > PAGE_SIZE - sizeof(struct page_control)){
        // working with pages for large chunks
        struct page_control_l* l_pg_ptr;
        if (large_chunks_page_pointer == NULL) {               // init pointer
            if (malloc_init_large_chunks(block_size) == -1){
                memory_location = NULL;
                goto MALLOC_END;
            }
            memory_location = (void *) large_chunks_page_pointer + sizeof(struct page_control_l);
            goto MALLOC_END;
        }

        memory_location = malloc_add_page_large_chunks(block_size) + sizeof(struct page_control_l);
        goto MALLOC_END;
    }

    MALLOC_END:
    pthread_mutex_unlock(&lock);
    return memory_location;
}

void* calloc(size_t number, size_t size){
    long PAGE_SIZE = sysconf(_SC_PAGESIZE);       // pagesize in the current system

    void* memory_location;
    void* current_addr;
    char* atomic;
    if ((! size) || (! number)){
        return NULL;
    }
    size_t array_size = number * size;
    memory_location = malloc(array_size);
    if (! memory_location){
        return NULL;
    }

    current_addr = memory_location;

    while (current_addr < memory_location + array_size){
        atomic = (char*) current_addr;
        *atomic = 0;
        current_addr += sizeof(char*);
    }

    return memory_location;
}

void* realloc(void* pointer, size_t new_size){
    long PAGE_SIZE = sysconf(_SC_PAGESIZE);       // pagesize in the current system

    void* memory_allocation;
    size_t old_size;
    if ((! pointer) && (! new_size)){
        return NULL;
    }
    else if ((! pointer) && new_size){
        return malloc(new_size);
    }
    else if (! new_size){
        free(pointer);
        return NULL;
    }

    // Check whether pointer in very_small_pages
    if (very_small_chunks_page_pointer){
        struct page_control_vs* vs_pg_ptr = (struct page_control_vs*) very_small_chunks_page_pointer;
        while (vs_pg_ptr){
            if (pointer > (void *) vs_pg_ptr + sizeof(struct page_control_vs) &&
                pointer < (void *) vs_pg_ptr + PAGE_SIZE){
                old_size = VERY_SMALL_CHUNKS_LIMIT;
                if (new_size <= VERY_SMALL_CHUNKS_LIMIT){
                    return pointer;
                }
                goto REALLOC_MALLOC;
            }
            vs_pg_ptr = (struct page_control_vs*) vs_pg_ptr->next_page;
        }
    }

    // Check whether pointer in small_pages
    if (small_chunks_page_pointer){
        struct page_control_s* s_pg_ptr = (struct page_control_s*) small_chunks_page_pointer;
        while (s_pg_ptr){
            if (pointer > (void *) s_pg_ptr + sizeof(struct page_control_s) &&
                pointer < (void *) s_pg_ptr + PAGE_SIZE){
                old_size = SMALL_CHUNKS_LIMIT;
                if (new_size <= SMALL_CHUNKS_LIMIT && new_size > VERY_SMALL_CHUNKS_LIMIT){
                    return pointer;
                }
                goto REALLOC_MALLOC;
            }
            s_pg_ptr = (struct page_control_s*) s_pg_ptr->next_page;
        }
    }

    // Check whether pointer in big_pages
    if (big_chunks_page_pointer){
        struct page_control* current_page;
        struct block_control* current_chunk;
        char found_in_big = 0;
        current_page = (struct page_control*) big_chunks_page_pointer;          // pointer to page
        while (current_page){
            current_chunk = (struct block_control*) current_page->used_blocks;
            while (current_chunk){
                if (pointer == (void *) current_chunk + sizeof(struct block_control)){
                    old_size = current_chunk->size;
                    if (current_chunk->size >= new_size && new_size > SMALL_CHUNKS_LIMIT){
                        return pointer;
                    }
                    goto REALLOC_MALLOC;
                }
                current_chunk = (struct block_control*) current_chunk->next_chunk;
            }
            if (found_in_big){
                break;
            }
            current_page = (struct page_control*) current_page->next_page;
        }
    }

    // Check whether pointer in large_pages
    if (large_chunks_page_pointer){
        struct page_control_l* current_page = (struct page_control_l*) large_chunks_page_pointer;
        struct page_control_l* temp_page = (struct page_control_l*) large_chunks_page_pointer;
        while (pointer != (void *) current_page + sizeof(struct page_control_l)
                && (current_page)){
            current_page = (struct page_control_l*) current_page->next_page;
        }
        if (current_page){
            old_size = current_page->size;
            if (new_size > PAGE_SIZE - sizeof(struct page_control_l) &&
                new_size < old_size / PAGE_SIZE + 1 - sizeof(struct page_control_l)){
                return pointer;
            }
            goto REALLOC_MALLOC;
        }

    }

    REALLOC_MALLOC:
    memory_allocation = malloc(new_size);
    if (! memory_allocation){
        return pointer;
    }

    void* current_addr_old = pointer;
    void* current_addr_new = memory_allocation;
    char* atomic_old;
    char* atomic_new;
    size_t limit;
    if (old_size < new_size){
        limit = old_size;
    }
    else {
        limit = new_size;
    }
    while (current_addr_old < pointer + new_size){
        atomic_old = (char*) current_addr_old;
        atomic_new = (char*) current_addr_new;
        *atomic_new = *atomic_old;
        current_addr_old += sizeof(char);
        current_addr_new += sizeof(char);
    }

    free(pointer);
    return memory_allocation;

}
