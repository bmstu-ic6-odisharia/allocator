# Makefile for pidalloc, malloc implementation

malloc: pidalloc.c
	gcc -fPIC -shared pidalloc.c -o pidalloc.so

debug: pidalloc.c
	gcc -fPIC -g -shared pidalloc.c -o pidalloc.so
